﻿using UnityEngine;
using System.Collections;

public static class BoardUtils
{
	public static Vector2 GetWorldPositionForBoardPosition(Vector2 boardPosition) {
		Vector2 boardOffset = new Vector2(-((BoardDefs.boardSizeWidth/2.5f)*CardDefs.CardSize), (BoardDefs.boardSizeHeight/2)*CardDefs.CardSize);
		return boardOffset+new Vector2 (boardPosition.x * CardDefs.CardSize, -boardPosition.y * CardDefs.CardSize);
	}
}
