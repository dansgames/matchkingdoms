﻿using UnityEngine;
using System.Collections;

public static class GameDefs {

	public enum DamageType {
		sword,
		bow,
		magic
	}

	public static float GetDamageForMatchPoints(DamageType type, int matchPoints) {
		switch (type) {
		case DamageType.sword:
			return matchPoints*3f;

		case DamageType.bow:
			return matchPoints*1.5f;
		}

		return 0f;
	}

	public static int GetEnemyAmountForWave(int wave) {
		return wave * wave;
	}

}
