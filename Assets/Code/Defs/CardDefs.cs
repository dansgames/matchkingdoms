﻿using UnityEngine;
using System.Collections;

public static class CardDefs {

	public enum eCardType {
		bow,
		shield,
		sword,
		PHp,
		PHp2
	}
	
	public const float CardSize = 0.45f;
	public const string CardPrefabName = "Cards/CardPrefab";
}