﻿using UnityEngine;
using System.Collections;

public static class BoardDefs {

	public enum eCardType {
		bow,
		shield,
		sword
	}
	
	public const float CardSize = 0.2f;
	public const string CardPrefabName = "CardPrefab";
	public const int boardSizeWidth = 5;
	public const int boardSizeHeight = 5;
}
