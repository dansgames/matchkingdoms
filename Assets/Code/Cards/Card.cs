﻿using UnityEngine;
using System.Collections;

public class Card : MonoBehaviour {

	#region private members

	private SpriteRenderer _mySpriteRenderer;
	private CardDefs.eCardType _myType;
	private Vector2 _boardPos;
	private bool _bFollowMouse;

	#endregion

	#region public interface

	public CardDefs.eCardType cardType { 
		get { return _myType; }
	}

	public Vector2 boardPosition {
		get { return _boardPos; }
	}

	#endregion

	#region MonoBehaviourInherited

	void Update() {
		if (_bFollowMouse) {
			Vector3 newPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
			newPos.z = -1;
			transform.position = newPos;
		}
	}

	#endregion

	#region public methods

	public void Setup(CardDefs.eCardType type, Vector2 startPos) {
		_mySpriteRenderer = GetComponentInChildren<SpriteRenderer> ();
		_myType = type;

		var sprite = Resources.Load<Sprite>("Cards/"+cardType.ToString());
		_mySpriteRenderer.sprite = sprite;

		_boardPos = startPos;
		gameObject.name = "bp_" + _boardPos;
	}

	public void Activate(bool active) {
		if (active) {
			_bFollowMouse = true;
		} else {
			_bFollowMouse = false;
			transform.localPosition = BoardUtils.GetWorldPositionForBoardPosition(boardPosition);
		}
	}

	public void TileSwapped(Vector2 newPosition) {
		_boardPos = newPosition;
		gameObject.name = "bp_" + _boardPos;
	}

	#endregion
}
