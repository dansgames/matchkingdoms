﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GameMaster : MonoBehaviour {

	#region Private Types

	enum eGameState {
		Paused,
		Ready
	}

	#endregion

	#region private variables

	private eGameState _state = eGameState.Paused;

	private InputMaster _input;
	private BoardMaster _board;
	private EnemyMaster _enemyMaster;
	private HUDMaster	_hud;

	private int _wave = 1;

	private int _defense = 20;

	// Positioning
	private Vector3 _boardPosition = new Vector3(-0.04f, -1.2f, 0);

	private Dictionary<CardDefs.eCardType, int> cardsCleared = new Dictionary<CardDefs.eCardType, int>();

	#endregion

	public void StartNewGame() {
		BeginWave();
		_state = eGameState.Ready;
	}

	// Use this for initialization
	void Start () {
		GameObject input = new GameObject ();
		input.name = "_input";
		_input = input.AddComponent<InputMaster> ();

		_input.SwipeRegisteredEvent += HandleSwipeRegisteredEvent;

		GameObject board = new GameObject ();
		board.name = "_board";
		_board = board.AddComponent<BoardMaster> ();
		_board.transform.position = _boardPosition;
		_board.SetupRandom ();

		GameObject enemySpawner = new GameObject ();
		enemySpawner.name = "_enemySpawner";
		_enemyMaster = enemySpawner.AddComponent<EnemyMaster> ();

		GameObject hud = new GameObject ();
		hud.name = "_hud";
		_hud = hud.AddComponent<HUDMaster> ();
		_hud.Init ();
	}

	#region private methods

	void BeginWave() {
		_enemyMaster.CreateWave (_wave);
		_hud.NewGame ();
		_hud.UpdateHud (_wave, _defense);
	}

	void CheckCardsCleared() {
		// Check Bows
		int bows = CheckCardsCleared (CardDefs.eCardType.bow);

		// Check Swords
		int swords = CheckCardsCleared (CardDefs.eCardType.sword);

		// Check Shields
		int shields = CheckCardsCleared (CardDefs.eCardType.shield);

		Debug.Log ("Bows cleared: " + bows);
		Debug.Log ("Swords cleared: " + swords);
		Debug.Log ("Shields cleared: " + shields);

		cardsCleared.Clear ();

		_enemyMaster.TakeDamage (GameDefs.GetDamageForMatchPoints (GameDefs.DamageType.sword, swords));
		_enemyMaster.TakeDamage (GameDefs.GetDamageForMatchPoints (GameDefs.DamageType.bow, bows));

		_defense += shields;
		_hud.UpdateHud (_wave, _defense);

		if (!_enemyMaster.bWaveComplete) {
			_enemyMaster.EnemyMasterMoveCompleteEvent += HandleEnemyMasterMoveCompleteEvent;
			_enemyMaster.Move ();
		} 

		if (_enemyMaster.bWaveComplete) {
			Debug.Log ("Wave Complete!");
			_wave++;
			_hud.UpdateHud (_wave, _defense);
			_enemyMaster.CreateWave (_wave);
			_state = eGameState.Ready;
		}
	}

	int CheckCardsCleared(CardDefs.eCardType type) {
		if (cardsCleared.ContainsKey (type)) {
			return cardsCleared [type];
		} else {
			return 0;
		}
	}

	#endregion

	#region Event Handlers

	void HandleEnemyMasterMoveCompleteEvent (int breached)
	{
		_enemyMaster.EnemyMasterMoveCompleteEvent -= HandleEnemyMasterMoveCompleteEvent;

		_defense -= breached;
		_hud.UpdateHud (_wave, _defense);

		Debug.Log ("Turn Complete - Breaches: "+breached);
		_state = eGameState.Ready;
	}

	void HandleSwipeRegisteredEvent (eSwipeDirection direction, Card card)
	{
		if (_state == eGameState.Paused)
			return;

		Debug.Log ("Got Swipe: " + direction);
		_state = eGameState.Paused;

		_board.MatchFailedEvent += HandleMatchFailedEvent;
		_board.MatchCompleteEvent += HandleMatchCompleteEvent;
		_board.MatchFoundEvent += HandleMatchFoundEvent;

		_board.TrySwapTile (card.boardPosition, direction);
	}

	void HandleMatchFoundEvent (CardDefs.eCardType typeOfMatch, int size)
	{
		Debug.Log ("Got Match Type: " + typeOfMatch + " size: " + size);

		if (cardsCleared.ContainsKey (typeOfMatch)) {
			int amount = cardsCleared [typeOfMatch];
			amount++;
			cardsCleared [typeOfMatch] = amount;
		} else {
			cardsCleared.Add(typeOfMatch, 1);
		}
	}

	void HandleMatchCompleteEvent ()
	{
		_board.MatchFailedEvent -= HandleMatchFailedEvent;
		_board.MatchCompleteEvent -= HandleMatchCompleteEvent;
		_board.MatchFoundEvent -= HandleMatchFoundEvent;

		CheckCardsCleared ();
	}

	void HandleMatchFailedEvent ()
	{
		_board.MatchFailedEvent -= HandleMatchFailedEvent;
		_board.MatchCompleteEvent -= HandleMatchCompleteEvent;
		_board.MatchFoundEvent -= HandleMatchFoundEvent;

		Debug.Log ("Match Failed");
		_state = eGameState.Ready;
	}

	#endregion
}
