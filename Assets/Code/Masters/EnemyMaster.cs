﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class EnemyMaster : MonoBehaviour {

	#region events

	public delegate void EnemyMasterMoveEventHandler (int enemiesBreaching);
	public event EnemyMasterMoveEventHandler EnemyMasterMoveCompleteEvent;

	#endregion

	#region public interface

	public bool bWaveComplete {
		get { return _bWaveComplete; }
	}

	#endregion

	#region private variables

	private List<GameObject> _enemies = new List<GameObject>();
	private bool _bWaveComplete = true;

	#endregion

	#region public methods

	public void CreateWave(int wave) {

		int enemyCount = GameDefs.GetEnemyAmountForWave (wave);

		int height = Mathf.CeilToInt((Mathf.Sqrt(enemyCount)));
		int width = enemyCount/height;

		for (int iy = 0; iy < height; iy++) {
			for (int ix = 0; ix < width; ix++) {

				Vector3 spawnPosition = EnemyDefs.enemySpawnStartPosition;

				float space = ix*EnemyDefs.enemySpacing;
				float offset = ((width-1f)/2f)*EnemyDefs.enemySpacing;

				spawnPosition.x += space-offset;
				spawnPosition.y += EnemyDefs.enemySpacing*iy;

				GameObject newEnemy = GameObject.Instantiate (Resources.Load (EnemyDefs.kEnemyPrefabName)) as GameObject;
				_enemies.Add (newEnemy);
				
				newEnemy.transform.position = spawnPosition;
			}
		}

		_bWaveComplete = false;
	}

	public void TakeDamage(float damage) {

		Debug.Log ("<color=cyan> Took Damage: "+damage+" </color>");

		for (int i=0; i<damage; i++) {

			if (_enemies.Count > 0) {
				Debug.Log ("<color=white> Destroying an enemy </color>");
				
				GameObject enemy = _enemies [0];
				_enemies.RemoveAt (0);
				GameObject.Destroy (enemy);

				if (_enemies.Count < 1)
					_bWaveComplete = true;

			} else {
				_bWaveComplete = true;
			}
		}
	}

	public void Move() {
		int breached = 0;
		
		for (int i=_enemies.Count-1; i>-1; i--) {
			Vector3 newPos = _enemies[i].transform.position;

			newPos.y -= EnemyDefs.enemyMoveDistance;

			_enemies[i].transform.position = newPos;

			if(_enemies[i].transform.position.y <= EnemyDefs.enemyAttackHeight) {
				breached++;
				GameObject enemy = _enemies[i];
				_enemies.RemoveAt(i);
				Destroy (enemy);
			}
		}
		
		if (_enemies.Count < 1)
			_bWaveComplete = true;
		
		if (EnemyMasterMoveCompleteEvent != null)
			EnemyMasterMoveCompleteEvent (breached);
	}

	#endregion
}
