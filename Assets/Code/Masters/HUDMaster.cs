﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class HUDMaster : MonoBehaviour {

	private Text _txtWave;
	private Text _txtDefense;

	private GameObject _startScreen;

	public void Init() {
		_txtWave = GameObject.Find ("txtWave").GetComponent<Text> ();
		if (!_txtWave)
			Debug.LogError (this + " CANNOT FIND txtWave. WILL NOT WORK.");

		_txtDefense = GameObject.Find ("txtDefense").GetComponent<Text> ();
		if (!_txtDefense)
			Debug.LogError (this + " CANNOT FIND txtDefense. WILL NOT WORK.");

		_startScreen = GameObject.Find ("StartScreen").gameObject;
		if (!_txtDefense)
			Debug.LogError (this + " CANNOT FIND StartScreen. WILL NOT WORK.");

		ShowHud (false);
	}

	public void NewGame() {
		ShowHud (true);
		_startScreen.SetActive (false);
	}

	public void ShowHud(bool toggle) {
		if (toggle) {
			_txtWave.enabled = true;
			_txtDefense.enabled = true;
		} else {
			_txtWave.enabled = false;
			_txtDefense.enabled = false;
		}
	}

	public void UpdateHud(int wave, int defense) {
		_txtWave.text = "Wave: " + wave;
		_txtDefense.text = "Defense: " + defense;
	}
}
